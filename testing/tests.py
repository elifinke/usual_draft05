#!/usr/bin/env python3.7
'''
Tests all GET routes (local or AWS server).

__main__ method tests all the GET routes. Use argv local if running local server and argv v5 to use the AWS-hosted version.
'''

import json, requests
import random

__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-07-03"
__updated__ = "2019-08-01"


ROOT_URLS = {
    # 'v4': 'https://t9litrciwd.execute-api.us-east-1.amazonaws.com/dev', # v4 was taken offline
    'v5': 'https://x9gctr0aac.execute-api.us-east-1.amazonaws.com/dev',
    'local': 'http://localhost:5000'
}


def get_url(url, params=None):
    '''Make request and return only relevant info'''
    req = requests.get(url, params=params)
    # print('content', json.loads(req.content), 'status code', req.status_code)
    return json.loads(req.content), req.status_code

def get_endpoint(ROOT_URL, endpoint, params=None):
    return get_url(ROOT_URL + endpoint, params)

"""
Individual tests
"""

def error_retrieving(page):
    print('Error retrieving ' + page +'. Server may be down.')

def tests_passed(page):
    print('Tests for ' + page + ' passed.')

def error_testing(page, data):
    print('Error (test failed) when testing ' + page + '. Data returned:\n'+str(data))

def preprocess(url_root, endpoint, page):
    data, status = get_endpoint(url_root, endpoint)
    if status is not 200:
        error_retrieving(page)
        return None
    return data

def process(url_root, endpoint, page, shouldbe):
    data = preprocess(url_root, endpoint, page)
    if data is None: return   
    if data['data']==shouldbe:
        tests_passed(page)
    else:
        error_testing(page, data)

def is_valid_id(obj):
    return isinstance(obj, int) and obj>0

def get_super():
    try:
        superinfo_meta = get_endpoint(url_root, '/api/users/super/')
        s_uid = str(int(superinfo_meta[0]['data']['uid']))
        s_meal_id = str(int(superinfo_meta[0]['data']['fmeal_id']))
        return s_uid, s_meal_id
    except:
        return [None]
    # except:
    #     s_uid = '10'
    #     s_meal_id = '20'
    




def test_landing(url_root):
    process(url_root, '/api/', 'landing page', 'Welcome!')


def test_dummy(url_root):
    process(url_root, '/api/dummy/', 'dummy data', ['DUMMY DATA'])

def test_all_uids(url_root):
    page = 'list of uids'
    data = preprocess(url_root, '/api/users/all-uids/', page)
    if data is None:
        error_testing(page, data)
        return   
    if isinstance(data['data'], list) and all(isinstance(i, int) for i in data['data']) and len(data['data'])>0:
        tests_passed(page)
        return data['data']
    else:
        error_testing(page, data)

def test_get_login(url_root):
    uids = test_all_uids(url_root)
    page = 'login page'
    if uids is None:
        print('Could not test', page)
        return
    uid = random.choice(uids)
    nouid = max(uids)+1
    user = preprocess(url_root, '/api/login/?uid='+str(uid), page)
    if user is None or user['data']['uid']!=uid:
        return error_testing(page, user)

    # nouser = get_endpoint('/api/login/?uid='+str(nouid))[0]
    nouser = get_endpoint(url_root, '/api/login/', params={'uid': str(nouid)})[0]

    if nouser['data']!='Could not find user':
        error_testing(page, nouser)
        print('data should have been: "User does not yet exist"')
        return
    tests_passed(page)

def ok_location_dict(loc):
    if not isinstance(loc, dict):
        return False
    if not is_valid_id(loc['id']):
        return False
    if not (isinstance(loc['lat'], float) and isinstance(loc['lng'], float)):
        return False
    name = loc.get('loc_name','') + loc.get('name','')
    return 'Chicago' in name or 'office kitchen' in name

def test_all_locations(url_root):
    page = 'list of locations'
    locs = preprocess(url_root, '/api/locations/', page)
    if locs is None or not isinstance(locs, dict):
        return error_testing(page, locs)
    locslist = locs.get('data', None)
    if not isinstance(locslist, list) or len(locslist)==0:
        return error_testing(page, locs)
    if not ok_location_dict(locslist[0]):
        return error_testing(page, locs)
    tests_passed(page)


def test_get_favmeal(url_root):
    page = 'meal recommendations'
    s_uid, s_meal_id = get_super()
    data = preprocess(url_root, '/api/favmeal/meal/?uid='+s_uid, page)
    if data is None or str(data['data']['meal']['id'])!=s_meal_id:
        # print(data['data']['meal']['id'])
        # print(type(data['data']['meal']['id']))
        return error_testing(page, data)
    data2 = preprocess(url_root, '/api/favmeal/meal/?uid=4&gps=0&loc_id=4', page)
    if data2 is None or data2['data']['suggest']!=False:
        return error_testing(page, data2)
    tests_passed(page)    
    
def valid_order(order):
    if not isinstance(order, dict):
        return False
    if not all(is_valid_id(order.get(key, None)) for key in ['id', 'uid', 'loc_id']):
        return False
    if not isinstance(order.get('contents', None), dict) or len(order['contents'])==0:
        return False
    if not isinstance(order.get('timestamp', None), str) or not isinstance(order.get('price', None), float):
        return False
    return True

def test_get_all_orders(url_root):
    page = 'all orders'
    data = preprocess(url_root, '/api/orders/all/', page)
    if page is None:
        return error_testing(page, data)
    if not isinstance(data, dict) or not isinstance(data.get('data', ''), list) or len(data['data'])==0:
        return error_testing(page, data)
    order = data['data'][0]
    if not valid_order(order):
        return error_testing(page, data)
    tests_passed(page)
    return order

def test_get_order(url_root):
    page = 'single order'

    order = test_get_all_orders(url_root)
    if order is None:
        return print('Could not test', page)
    orderid = order['id']

    process(url_root, '/api/orders/?id='+str(orderid), page, order)


def test_favmeal_dash_users(url_root):
    page = 'list of "users with a recommended meal here"'
    s_uid = get_super()[0]

    uiddata, status = get_endpoint(url_root, '/api/users/all-uids/')
    if status is not 200:
        error_retrieving(page)
        return print('unable to test ' + page + 'because could not retrieve list of uids')
    uids = uiddata['data']
    if len(uids)==0:
        return print('unable to test ' + page + 'because could not retrieve list of uids')
    locdata, status = get_endpoint(url_root, '/api/locations/')
    if status is not 200:
        error_retrieving(page)
        return print('unable to test ' + page + 'because could not retrieve list of locations')
    locs = list(map((lambda x: x['id']), locdata['data']))
    if len(locs)==0:
        return print('unable to test ' + page + 'because could not retrieve list of locations')

    no_rec = None
    yes_rec = None
    for loc_id in locs:
        recs, status = get_endpoint(url_root, '/favmeal/users/', params={'loc_id': loc_id, 'gps': 0})
        if status is not 200:
            return error_retrieving(page)
        if recs['data']==[] and no_rec is None:
            no_rec = loc_id
        # print('users listed', recs['data'], 'loc_id', loc_id)
        if recs['data']!=[] and yes_rec is None:
            yes_rec = (loc_id, recs['data'][0])
    
    if no_rec is None:
        print('No locations found with no recommendations, so couldn\'t test that aspect')
    else:
        test_component = 'confirming no recs for location with empty list'
        for uid in uids:
            if str(uid)!=s_uid:
                recs, status = get_endpoint(url_root, '/api/favmeal/meal/', params={'loc_id': no_rec, 'uid': uid, 'gps': 0})
                if status is not 200 or recs['data']['suggest'] is True:
                    return error_testing(page, {'test component': test_component, 'recs': recs, 'status': status})
    if yes_rec is None:
        print('No locations found with any recommendations, so couldn\'t test that aspect')
    else:
        assert isinstance(yes_rec, tuple)
        recs, status = get_endpoint(url_root, '/api/favmeal/meal/', params={'loc_id': yes_rec[0], 'uid': yes_rec[1], 'gps': 0})
        if status is not 200 or recs['data']['suggest'] is False:
            return error_testing(page, {'test component': test_component, 'recs': recs, 'status': status})
    
    return tests_passed(page)
    

"""
Organize testing
"""

test_mapper = {
        '/api/': test_landing, 
        '/api/dummy/': test_dummy, 
        '/api/favmeal/meal/': test_get_favmeal,
        '/api/locations/': test_all_locations,
        '/api/login/': test_get_login,
        '/api/orders/': test_get_order,
        '/api/orders/all/': test_get_all_orders,
        '/api/users/all-uids/': test_all_uids
    }

def which_to_test(url_root):
    print('\ndetermining which endpoints to test.\n')
    to_test = []
    response = get_endpoint(url_root, endpoint='/api/options/')
    options, status = response
    if status is not 200:
        print('\nerror retrieving options. server may be down.')
        print('aborting tests.\n')
        return to_test
    # print('Options:', options['data']['Options'])
    for endpoint in options['data']['Options']:
        if test_mapper.get(endpoint, None) is not None:
            to_test.append(endpoint)
    return to_test

def redundant(test, to_test):
    if test is '/api/users/all-uids/' and '/api/login/' in to_test:
        return True
    if test is '/api/orders/all/' and '/api/orders/' in to_test:
        return True
    return False

def autotest(url_root):
    to_test = which_to_test(url_root)
    # print('Will test', to_test)
    for test in to_test:
        if not redundant(test, to_test):
            test_mapper[test](url_root)
    print('')
    test_favmeal_dash_users(url_root)
    print('\n\nFinished testing.\n\n')


if __name__=='__main__':
    try:
        import sys
        version = sys.argv[1]
        url_root = ROOT_URLS[version]
        ok = True
    except:
        print('Which version? options are')
        print(', '.join(ROOT_URLS.keys()))
        ok = False
    if ok:
        try:
            autotest(url_root)
        except Exception as e:
            print('Test errored.')
            print('Issue:')
            print(repr(e))
            print(e)
            