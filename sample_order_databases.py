#!/usr/bin/env python3.7
'''
Sample order databases and printing functionality.

For demonstration purposes.

Contains some sample order databases; use __main__ method to generate and print out meal recommendations.

Use argv -t <int> to test order number <int>, or -t r to use the order database in S3.
'''

import pandas as pd
import time, math, datetime


import basic_ml
import repo
import helper_tests
import hardcoded_res


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-07-26"
__updated__ = "2019-08-01"


orders1 = basic_ml._what_do_I_need()['example']

orders2 = [
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': "2019-02-25 07:55:00-05:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': "2019-02-28 08:55:00-05:00", 'uid': 4}
]

orders3 = [
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': "2019-02-25 07:55:00-05:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': "2019-02-28 07:55:00-06:00", 'uid': 4}
]

orders4 = [
    {'contents': {'Jelly Sandwich': 1, 'Small Sprite': 1}, 'loc_id': 3, 'timestamp': "2019-02-20 08:02:00-05:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-20 13:30:00-06:00", 'uid': 4},
    {'contents': {'Jelly Sandwich': 1, 'Small Sprite': 1}, 'loc_id': 3, 'timestamp': "2019-02-22 07:55:00-05:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-23 11:55:00-06:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-24 12:14:00-06:00", 'uid': 4}
]

orders5 = [
    {'contents': {'Medium Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-16 12:05:00-06:00", 'uid': 4},
    {'contents': {'Jelly Sandwich': 1, 'Small Sprite': 1}, 'loc_id': 3, 'timestamp': "2019-02-17 11:49:00-06:00", 'uid': 4},
    {'contents': {'Medium Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-18 13:22:00-06:00", 'uid': 4},
    {'contents': {'Jelly Sandwich': 1, 'Small Sprite': 1}, 'loc_id': 3, 'timestamp': "2019-02-19 12:02:00-06:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-20 13:30:00-06:00", 'uid': 4},
    {'contents': {'Jelly Sandwich': 1, 'Small Sprite': 1}, 'loc_id': 3, 'timestamp': "2019-02-22 12:55:00-06:00", 'uid': 4},
    {'contents': {'Medium Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-23 11:55:00-06:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-24 12:14:00-06:00", 'uid': 4}
]

orders6 = [ # now
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': str(datetime.datetime.now()-datetime.timedelta(days=7, minutes=-124)), 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': str(datetime.datetime.now()-datetime.timedelta(days=5, minutes=-20)), 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': str(datetime.datetime.now()-datetime.timedelta(days=4, minutes=30)), 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': str(datetime.datetime.now()-datetime.timedelta(days=2, minutes=-10)), 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 3, 'timestamp': str(datetime.datetime.now()-datetime.timedelta(days=1, minutes=15)), 'uid': 4}
]

t_around_now = [
    (datetime.datetime.now()-datetime.timedelta(days=4, minutes=30)).time(),
    (datetime.datetime.now()-datetime.timedelta(days=2, minutes=-10)).time()
]
str_round_t_list = lambda ls: list(map((lambda x: str(x.replace(second=0, microsecond=0))), ls))
t_around_now = str_round_t_list(t_around_now)
orders7 = [ # different timezone
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-20 "+t_around_now[0]+"-09:00", 'uid': 4},
    {'contents': {'Big Mac': 1}, 'loc_id': 2, 'timestamp': "2019-02-24 "+t_around_now[1]+"-09:00", 'uid': 4}
]


orders8 = [
    {'contents': {'Bacon Ranch Salad': 1}, 'loc_id': 3, 'timestamp': "2019-04-21 08:50:00-05:00", 'uid': 4},
    {'contents': {'Bacon Ranch Salad': 1}, 'loc_id': 3, 'timestamp': "2019-04-23 08:55:00-05:00", 'uid': 4},
    {'contents': {'Bacon Ranch Salad': 1}, 'loc_id': 3, 'timestamp': "2019-04-25 08:42:00-05:00", 'uid': 4}
]



def real_fake_data():
    '''Retrieve order database and return json serialization'''
    return repo.s_o_df(repo.download('order'))

orders = [orders1, orders2, orders3, orders4, orders5, orders6, orders7, orders8]


def pr(slow):
    '''Newlines with optional dramatic pauses'''
    print('\n')
    if slow:
        time.sleep(.2)


def test_new(orders, slow=True, longhist=False):
    '''
    Generate and print recommendations given fake order table.
    Order table is list of dicts.
    '''
    assert isinstance(orders, list), 'Order table should be a list of dicts'
    assert len(orders)>0, 'Order table should be a list of dicts'
    assert isinstance(orders[0], dict), 'Order table should be a list of dicts'
    pr(slow)
    print('This is the order history:')
    df = pd.DataFrame(orders)
    if df.shape[0] > 5 and not longhist:
        print('...')
        print(df.tail())
    else:
        print(df)
    if slow:
        time.sleep(.2)
    res = basic_ml._testing__generate_some_fav_meals(orders)
    pr(slow)
    print('basic_ml generated the following recommendations:')
    if slow:
        time.sleep(.2)
    print(res['current_fmeals'])
    return res


def test_and_print(argv):
    '''Handle argv and control sequence'''
    starttime = time.time()
    res = None
    args = ' '.join(argv)
    slow = 'slow' in args
    longhist = 'long' in args
    for i in range(1, len(orders)+1):
        if '-t %d'%(i) in args:
            res = test_new(orders[i-1], slow, longhist)
    if '-t r' in args:
        res = test_new(real_fake_data(), slow, longhist)
    print('\n')
    if res is not None:
        print('Generated in {0:.1f} seconds.'.format(0.1*math.ceil(10*(time.time()-starttime))))
        if 'time' in args:
            for f in res['new_favs']:
                print('\n')
                print(f)
                helper_tests.test_meal_time(f)
                print_range(f)
                # one_stdev_s, one_stdev_e = repo.calculate_stdev_time_range(f, 1)
                # print('With cutoff at %.2f stdevs, would be recommended between %s and %s'%(1, one_stdev_s, one_stdev_e))
                # custom_cutoff = hardcoded_res.STDEV_CUTOFF
                # print('custom',custom_cutoff)
                # if custom_cutoff!=1:
                #     c_stdev_s, c_stdev_e = repo.calculate_stdev_time_range(f, custom_cutoff)
                #     print('With cutoff at %d stdevs, would be recommended between %s and %s'%(custom_cutoff, c_stdev_s, c_stdev_e))
    else:
        print('Specify an order history to test; use argv `-t <INT>` (indexed starting at 1)')
    print('\n')


def print_range(f):
    '''print additional time-related info for recommendations'''
    one_stdev_s, one_stdev_e = repo.calculate_stdev_time_range(f, 1)
    print('With cutoff at %.2f stdevs, would be recommended between %s and %s'%(1, one_stdev_s, one_stdev_e))
    custom_cutoff = hardcoded_res.STDEV_CUTOFF
    if custom_cutoff!=1:
        c_stdev_s, c_stdev_e = repo.calculate_stdev_time_range(f, custom_cutoff)
        print('With cutoff at %.2f stdevs, would be recommended between %s and %s'%(custom_cutoff, c_stdev_s, c_stdev_e))



if __name__ == "__main__":
    from sys import argv
    test_and_print(argv)