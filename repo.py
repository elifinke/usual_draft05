#!/usr/bin/env python3.7
'''
Data manipulation and some business logic.

"Getters" and "setters" for table data.
'''


import datetime
import json, requests
import dateutil.parser as dtread
import math, numpy
import pandas as pd
from parse import search

import hardcoded_res
import s3_data_access


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-06-20"
__updated__ = "2019-08-01"

"""
Database access
"""

def download(names):
    '''Given filename, download data and return dataframe'''
    data = s3_data_access.getpandas(names)
    return data
    
def upload(name, data):
    '''Given filename, try to upload data'''
    try:
        s3_data_access.publish(name, data)
        return True
    except:
        return False



"""
helper methods - data formatting
"""

def extract_time_from_object(obj):
    '''Given a string, time, or datetime object, return a time object'''
    if isinstance(obj, str):
        obj = extract_date(obj)
    if isinstance(obj, datetime.datetime):
        return obj.time()
    if isinstance(obj, datetime.time):
        return obj
    return None

def extract_date(string):
    """Given a string, return a gnostic datetime object"""
    if isinstance(string, str):
        return dtread.parse(string).replace(tzinfo=datetime.timezone.utc)
    if isinstance(string, datetime.datetime):
        return string.replace(tzinfo=datetime.timezone.utc)
# These two are bijective so that's nice!
def s_time(dobj):
    """Given a gnostic time or datetime object, return a string"""
    return str(dobj)

def convert_tdelta_str_to_tdelta(string):
    '''Create and return a datetime.timedelta object, given its string representation'''
    if string is None:
        return datetime.timedelta(seconds=0)
    if string is math.inf:
        return datetime.timedelta.max 
    return dtread.parse(string) - datetime.datetime.now().replace(hour=0, minute=0, second=0)



def nextid(table, idkey='id'):
    '''table is a dataframe; return next index''' # because I don't trust built in pandas functionality
    return max(table[idkey].tolist()) + 1




def getrow(table, expr):
    '''Return a series for first row matching the expression'''
    if table is None:
        return None
    rows = table.query(expr)
    if rows.empty:
        return None
    else:
        return rows.iloc[0]

def getrows(table, expr):
    '''Return a list of all serieses that match the expression'''
    if table is None:
        return None
    rows = table.query(expr)
    return get_all_rows(rows)

def get_all_rows(table):
    '''Return a list of all serieses in table'''
    if table is None:
        return None
    data = []
    for i in range(table.shape[0]):
        data.append(table.iloc[i])
    return data



# def around_now(tstring, allowable_diff_minutes=None):
#     now = datetime.datetime.now(datetime.timezone.utc).time()
#     return close_in_time(tstring, now, allowable_diff_minutes)


def time_obj_difference(tobj1, tobj2):
    '''tobj1 and tobj2 must be (naive?) datetime.time objects
    return a datetime.timedelta object'''
    sample_day = datetime.date(year = 2000, month=3, day=3)
    one_day = datetime.timedelta(days=1)
    d1 = datetime.datetime.combine(sample_day, tobj1)
    d2 = datetime.datetime.combine(sample_day, tobj2)
    diff = min([abs(d1-d2), abs(d1-d2-one_day), abs(d1-d2+one_day)]) # so times at 23:50 and 00:10 are 'close'
    return diff


# def close_in_time(tstring1, tstring2, allowable_diff_minutes=None):
#     d1, d2 = extract_time_from_object(tstring1), extract_time_from_object(tstring2)
#     diff = time_obj_difference(d1, d2)

#     if isinstance(allowable_diff_minutes, datetime.timedelta):
#         allowable_diff = allowable_diff_minutes
#     else:
#         if allowable_diff_minutes is None:
#             allowable_diff_minutes = hardcoded_res.MIN_WITHIN
#         allowable_diff = datetime.timedelta(minutes = allowable_diff_minutes)

#     return diff < allowable_diff


def time_within_stdev(meal, curr_local_time = None, cutoff=None):
    '''Is the current time within "cutoff" standard deviations of meal recommendation time?'''
    if cutoff is None:
        cutoff = hardcoded_res.STDEV_CUTOFF
    return (time_stdevs_off(meal, curr_local_time) <= cutoff)


def time_stdevs_off(meal, curr_local_time = None):
    '''
    How many standard deviations between the current local time and the meal recommendation time?
    I guess curr_local_time is an aware dt.time obj
    '''
    loc = s_loc(get_loc(meal['primary_loc']))
    if curr_local_time is None:
        try:
            curr_local_time = gdtime_from_lat_lng(loc['lat'], loc['lng']).time()
        except:
            return math.inf
    avg_time, std_time = extract_time_from_object(meal['time']), convert_tdelta_str_to_tdelta(meal.get('stdev_time', str(datetime.timedelta.resolution)))
    diff = time_obj_difference(curr_local_time, avg_time) 
    return abs(diff / std_time)

def calculate_stdev_time_range(meal, cutoff):
    '''
    meal is a dict; cutoff is an int
    return: string representation of start and end times of meal recommendation period
    '''
    avg_time, std_time = extract_time_from_object(meal['time']), convert_tdelta_str_to_tdelta(meal.get('stdev_time', str(datetime.timedelta.resolution)))
    sample_day = datetime.date(year = 2000, month=3, day=3)
    avg_time = datetime.datetime.combine(sample_day, avg_time)
    start_of_range = (avg_time - (std_time*cutoff)).replace(second=0, microsecond=0).time()
    end_of_range = (avg_time + (std_time*cutoff)).replace(second=0, microsecond=0).time()
    return str(start_of_range), str(end_of_range)


def loc_id_from_lat_lng(lat, lng):
    '''
    Given coordinates of McD location, find the actual location id
    '''
    loc_table = download('loc')
    if loc_table is 'Empty':
        return None
    locs = get_all_rows(loc_table)
    LMOE = hardcoded_res.LOCATION_MARGIN_OF_ERROR
    relevant_locs = list(filter((lambda loc: abs(loc.lat-lat)<LMOE and abs(loc.lng-lng)<LMOE), locs))
    if len(relevant_locs)==1:
        return int(relevant_locs[0].id)
    else:
        return None


def gdtime_from_lat_lng(lat, lng):
    '''
    Given coordinates, find the local time and return aware datetime object
    Can only run about a thousand times per hour!
    '''
    access_info = hardcoded_res.GEO_API_ACCESS_INFO()
    urlroot = 'http://api.geonames.org/timezoneJSON?'
    url = urlroot + 'lat=' + str(lat) + '&lng=' + str(lng) + '&' + access_info
    info = requests.get(url)
    if info.status_code is not 200:
        return
    timejson = info.json()
    tstring, dstOffset, timezoneName = timejson.get('time'), timejson['dstOffset'], timejson.get('timezoneId', '')
    if tstring is None:
        rawOffset = timejson.get('rawOffset')
        tobj = datetime.datetime.now(tz=datetime.timezone(datetime.timedelta(hours=rawOffset)))
    else:
        tobj = search('{:ti}', tstring)[0]
        tzinfo = datetime.timezone(datetime.timedelta(hours=dstOffset), name=timezoneName)
        tobj = tobj.replace(tzinfo=tzinfo, second=datetime.datetime.now().second)
    return tobj




"""
json serializers
"""

def s_user(series):
    '''
    Input: series for one user
    Return: json serialization for that user
    '''
    if series is None:
        return None
    raw = series.to_dict()
    ser = {}
    for i in ['uid', 'uname', 'creation_time']:
        ser[i] = raw[i]
        if isinstance(ser[i], numpy.int64):
            ser[i]=int(ser[i])
        if i is 'uid':
            ser[i] = int(ser[i])
    ser['creation_time'] = s_time(ser['creation_time'])
    return ser

def s_order(series):
    '''
    Input: series for one order
    Return: json serialization for that order
    '''
    if series is None:
        return None
    raw = series.to_dict()
    ser = {}
    for i in ['id', 'uid', 'timestamp', 'price', 'loc_id', 'contents']: # , 'location']:
        ser[i] = raw[i]
        if isinstance(ser[i], numpy.int64):
            ser[i]=int(ser[i])
        if i in ['id','uid']:
            ser[i] = int(ser[i])
    ser['timestamp'] = s_time(ser['timestamp'])
    if isinstance(ser['contents'], str):
        ser['contents'] = json.loads(ser['contents'])
    return ser

def s_o_df(df):
    '''Given a whole order dataframe, return a json serialization'''
    return [s_order(o) for o in get_all_rows(df)]

def s_loc(series):
    '''
    Input: series for one location
    Return: json serialization for that location
    '''
    if series is None:
        return None
    raw = series.to_dict()
    ser = {}
    for i in ['id', 'lat', 'lng', 'loc_name', 'utc_offset']:
        ser[i] = raw[i]
        if i is 'utc_offset':
            ser[i] = json.loads(ser[i])
        if isinstance(ser[i], numpy.int64):
            ser[i]=int(ser[i])
        if i is 'id':
            ser[i] = int(ser[i])

    return ser

def s_fmeal(series):
    '''
    Input: series for one recommended meal
    Return: json serialization for that recommended meal
    '''
    if series is None:
        return None
    raw = series.to_dict()
    ser = {}
    for i in ['id', 'uid', 'contents', 'time', 'primary_loc', 'stdev_time', 'weight']:
        ser[i] = raw.get(i)
        if isinstance(ser[i], numpy.int64):
            ser[i]=int(ser[i])
        if i in ['id', 'uid', 'primary_loc']:
            ser[i] = int(ser[i])
    ser['time'] = s_time(ser['time'])
    if isinstance(ser['contents'], str):
        ser['contents'] = json.loads(ser['contents'])
    return ser


def make_special_df(cols={}):
    '''Makes a new dataframe with the appropriate attributes/formatting'''
    df = pd.DataFrame()
    df.index.rename('key', inplace=True)
    for c in cols.keys():
        df[c] = cols[c]
    return df
    # df['contents'], df['id'], df['primary_loc'], df['time'], df['uid'], df['weight'], df['stdev_time'] = '', 0, 0, '', 0, 0, ''



"""
Query & modify databases
"""

def make_new_user(uname, dtime):
    '''Make user obj with appropriate username and creation time'''
    users = download('user')
    if users is 'Empty':
        users = make_special_df({'uid': 0, 'uname': '', 'creation_time': '', 'email': '', 'password': '', 'payment_info': ''})
        uid = 1
    else:
        uid = nextid(users, idkey='uid')

    info = {'uid': uid, 'uname': uname, 'creation_time': dtime}
    info['email'], info['password'], info['payment_info'] = '','',''
    info = pd.Series(info, name=users.shape[0])
    users = users.append(info)
    success = upload('user', users)
    if success:
        return True, find_user(uid)
    return False, 'Could not create user'

def find_user(uid):
    '''Download user database and return pd series for user with uid `uid`'''
    users = download('user')
    if users is 'Empty':
        return None
    return getrow(users, 'uid==%s'%uid)

def get_all_users():
    '''Download user database and return list of all pd serieses'''
    users = download('user')
    if users is 'Empty':
        return None
    return get_all_rows(users)

def get_all_uids():
    '''Download user database and return list of all uids'''
    users = download('user')
    if users is None or users is 'Empty':
        return None
    return users.uid.to_list()


def make_location(lat, lng, loc_name):
    '''Register McD location (ie make location obj) given coordinates and name'''
    locs = download('loc')

    if locs is 'Empty':
        locs = make_special_df({'id': 0, 'lat': 0, 'lng': 0, 'loc_name': '', 'utc_offset': ''})
        nid = 1
    else:
        nid = nextid(locs)

    tz = gdtime_from_lat_lng(lat, lng)
    tzinfo = datetime.datetime.strftime(tz, '%z')
    # datetime.datetime.strptime(string, '%z').tzinfo # recover timezone this way
    # tzinfo = str(tz.timetz().isoformat())
    info = {'id': nid, 'lat': lat, 'lng': lng, 'loc_name': loc_name, 'utc_offset': json.dumps(tzinfo)}
    series = pd.Series(info, name=locs.shape[0])
    locs = locs.append(series)

    success = upload('loc', locs)
    if success:
        return True, get_loc(nid)
    return False, 'Could not create location'

def get_loc(lid):
    '''Download location database and return pd series for location with id `lid`'''
    locs = download('loc')
    if locs is 'Empty':
        return None
    return getrow(locs, 'id==%s'%lid)

def get_locations():
    '''Download location database and return list of all pd serieses'''
    locs = download('loc')
    if locs is 'Empty':
        return None
    return get_all_rows(locs)

def get_all_loc_ids():
    '''Download location database and return list of all ids'''
    locs = download('loc')
    if locs is None or locs is 'Empty':
        return None
    return locs.id.to_list()


def make_order(uid, contents, price, loc_id, dtime):
    '''Add order to order history database given relevant data'''
    if find_user(uid) is None:
        return False, 'No user with this uid'
    if get_loc(loc_id) is None:
        return False, 'No location with this id'
    orders = download('order')
    if orders is None:
        return False, 'Unable to access order table'

    if orders is 'Empty':
        orders = make_special_df({'id':0, 'uid': 0, 'contents': '[]', 'price': 0, 'loc_id': 0, 'timestamp': ''})
        oid = 1
        # orders = pd.DataFrame(info, index=[0])
    else:
        oid = nextid(orders)

    info = {'uid': uid, 'contents': json.dumps(contents), 'price': price, 'loc_id': loc_id, 'timestamp': str(dtime)}
    info['id'] = oid
    series = pd.Series(info, name=orders.shape[0])
    orders = orders.append(series)

    success = upload('order', orders)
    if success:
        return True, get_order(oid)
    return False, 'Could not create order'



def get_order(oid):
    '''Download order history database and return pd series for order with id `id`'''
    orders = download('order')
    if orders is 'Empty':
        return None
    return getrow(orders, 'id==%s'%oid)

def get_all_orders():
    '''Download order history database and return list of all pd serieses'''
    orders = download('order')
    if orders is 'Empty':
        return None
    return get_all_rows(orders)

def get_all_orders_pd():
    '''Download order history database and return pd dataframe'''
    orders = download('order')
    if orders is 'Empty':
        return None
    return orders

def get_user_orders(uid):
    '''Download order history, find all orders pertaining to user with uid `uid`, and return list of serieses'''
    orders = download('order')
    if orders is 'Empty':
        return None
    return getrows(orders, 'uid==%s'%uid)

def get_user_loc_orders(uid, loc_id):
    '''Download order history, find all orders pertaining to user with uid `uid` and location with id `loc_id`, and return list of serieses'''
    orders = download('order')
    if orders is 'Empty':
        return None
    return getrows(orders, 'uid==%s & loc_id==%s'%(uid, loc_id))


def push_fav_meal_to_cloud(uid, contents, weight, loc, time, stdev_time):
    '''
    Add meal recommendation to meal recommendation database given data
    (note that basic_ml directly accesses/modifies database, so any changes to database access must be made there as well)
    '''
    if find_user(uid) is None:
        return False, 'No user with this uid'
    if get_loc(loc) is None:
        return False, 'No location with this id'
    fmeals = download('fmeal')
    if fmeals is None:
        return False, 'Could not access meal recommendation table'

    if fmeals is 'Empty':
        fmeals = make_special_df({
            'id': 0,
            'uid': 0,
            'contents': '[]',
            'weight': 0,
            'primary_loc': 0,
            'time': '',
            'stdev_time': ''
        })
        nid = 1
    else:
        nid = nextid(fmeals)

    info = {
        'id': nid,
        'uid': uid,
        'contents': json.dumps(contents),
        'weight': weight,
        'primary_loc': loc,
        'time': time,
        'stdev_time': stdev_time
    }
    series = pd.Series(info, name=fmeals.shape[0])
    fmeals = fmeals.append(series)

    success = upload('fmeal', fmeals)
    if success:
        return True, info
    return False, 'Could not create meal recommendation'


def get_user_loc_fav_meals(uid=None, loc_id=None):
    '''
    Can answer 2 questions depending on whether uid is supplied:
    "For this user at this location, what (if any) is the recommendation?" # uid -> int
    "For all users at this location, which have any recommendation?" # uid -> None
    loc_id is required.
    Downloads meal recommendation database.
    If uid is None, return a list of uids
    If uid is not None, return a list of serieses (may be empty)
    '''
    fav_meals = download('fmeal')
    if fav_meals is 'Empty':
        return None
    if uid is None:
        matches = getrows(fav_meals, 'primary_loc==%s'%(loc_id))
    else:
        matches = getrows(fav_meals, 'uid==%s & primary_loc==%s'%(uid, loc_id))
    loc = get_loc(loc_id)
    if loc is None:
        return None
    tz = datetime.datetime.strptime(json.loads(loc['utc_offset']), '%z').tzinfo
    cur_loc_t = datetime.datetime.now(tz).time()
    matches = list(filter((lambda m: time_within_stdev(m, curr_local_time=cur_loc_t)), matches))
    if uid is None:
        matches = {int(m['uid']) for m in matches}
        return sorted(list(matches))
    else:
        matches.sort(key=(lambda meal: meal.weight), reverse=True)
        matches.sort(key=(lambda m: time_stdevs_off(m, cur_loc_t)), reverse=False)
        return matches



def get_all_fav_meals():
    '''Download meal recommendations. Return list of serieses'''
    meal_table = download('fmeal')
    if meal_table is 'Empty':
        return None
    return get_all_rows(meal_table)

def get_all_fav_meals_pd():
    '''Download meal recommendations. Return pd DataFrame'''
    meal_table = download('fmeal')
    if meal_table is 'Empty':
        return None
    return meal_table

def get_fmeal(mid):
    '''Download meal recommendations. Return series with id `mid`'''
    fmeal = download('fmeal')
    if fmeal is 'Empty':
        return None
    return getrow(fmeal, 'id==%s'%mid)




def store_data_copies_on_s3():
    '''Cache all databases in S3'''
    return s3_data_access.store_data_copies_on_s3()

