#!/usr/bin/env python3.7
'''
Download and upload the data from AWS S3.
'''


import boto3
import pandas as pd
import time

import personal_data


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-06-26"
__updated__ = "2019-08-01"


'''
Hardcoded query info
'''

csv_name = {'user': 'user', 'order': 'order', 'loc': 'location', 'fmeal': 'fav_meal'} # 'floc': 'fav_location'



'''
Helper functions to manipulate metadata
'''

def gen_table_list(tables=None):
    '''
    Return a standardized list of table lookup names, given varying input formats
    '''
    if isinstance(tables, list):
        toget = []
        for t in tables:
            if t in csv_name.keys():
                toget.append(t)
    elif tables in csv_name.keys():
        toget = [tables]
    elif tables is None:
        toget = csv_name.keys()
    else: toget = []
    return toget


def _name_from_table(table):
    '''Given standard database nickname, return actual filename'''
    file_name = csv_name.get(table)
    if file_name is None:
        return None
    return file_name + '.csv'



'''
Functions to download data
'''

def get_personal_data():
    '''Personal login data'''
    return personal_data.secrets, personal_data.bucket_name


def getdata(tablename=None):
    '''
    table should be None, or a string.
    If tablename is a string, return the corresponding aws_bucket file (or None).
    If tablename is None, all files will be downloaded - 
        return a dictionary with keys=csvnames, values=aws_bucket files
    '''
    secrets, bucket_name = get_personal_data()
    client = boto3.client(
        's3',
        # Hard coded strings as credentials, not recommended.
        aws_access_key_id=secrets['key'],
        aws_secret_access_key=secrets['secret_key']
    )

    if tablename is None:
        csv_list = gen_table_list() # [_name_from_table(t) for t in gen_table_list()] 
        return {c: getdata(_name_from_table(c)) for c in csv_list}

    csv_name = _name_from_table(tablename)
    if csv_name is None:
        return None
    try:
        return client.get_object(Bucket=bucket_name, Key=csv_name)
    except:
        return None



def getpandas(tablename):
    """
    tablename should be a string (specifically, one of the filename aliases in csv_name.keys()).
    return a pandas DataFrame.
    """
    csvdata = getdata(tablename)
    if csvdata is None:
        # print('csvdata is None')
        return None
    if csvdata.get('ContentLength')<=2:
        data = 'Empty'
    else:
        idkey='key'
        data = pd.read_csv(csvdata.get('Body'), index_col=idkey)
    return data


'''
Upload data
'''

def publish(name, data):
    '''
    Upload table "data" as a csv to file with alias "name"
    '''
    try:
        return _publish(_name_from_table(name), data.to_csv(index='key')) # index=False deletes index
    except:
        return False

def _publish(filename, data):
    '''Update data `data` with filename `filename`'''
    secrets, bucket_name = get_personal_data()
    try:
        client = boto3.client(
            's3',
            # Hard coded strings as credentials, not recommended.
            aws_access_key_id=secrets['key'],
            aws_secret_access_key=secrets['secret_key']
        )

        client.put_object(Bucket=bucket_name, Key=filename, Body=data)

        return True
    except:
        return False


def store_data_copies_on_s3():
    '''Cache all databases in S3'''
    now = str(round(time.time()))
    gucci = {}
    for key in csv_name.keys():
        filename = 'backup_copies/' + now + '_' + _name_from_table(key)
        data = getdata(key)
        if data is None:
            gucci[key] = False
        else:
            data = data[key]['Body'].read()
            # gucci[key] = [filename, str(type(data))]
            gucci[key] = _publish(filename, data)
    return gucci

