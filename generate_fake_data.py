#!/usr/bin/env python3.7
'''
__main__ method generates random order data and uploads it to order history database.

Leave me in the same directory as "hardcoded_res" so I can copy the menu from there!

the file has some data used to introduct "bias" - real users are not equally likely to order all possible meals or from all possible locations.
the order's timestamp will be a random time in 2019. Date is prior to "now"; time is in between 6am and midnight local time.
use argv update to update meal recommendations
use any int as an argv if you want more than 1 fake order

Additional functions make_users and make_locations make random users and locations.
'''



import json
import requests
import random, string, datetime, time, math
import pandas as pd

import repo


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-06-25"
__updated__ = "2019-08-01"


# ROOT_URL = 'https://5tvw5wvm2l.execute-api.us-east-1.amazonaws.com/dev'
ROOT_URL = 'http://localhost:5000' # local
# ROOT_URL = 'https://x9gctr0aac.execute-api.us-east-1.amazonaws.com/dev' # v5




'''
Create menu
'''
from hardcoded_res import MENU_ITEMS


'''
Helper functions
'''

ERAS = {'B': (5,11), 'L': (11,16), 'D': (16,23)}
def get_era(dtobj):
    '''Not used. Could differentiate between times of dat'''
    era = 'X'
    for test in ERAS.keys(): # should do .values
        t1 = ERAS[test][0]
        t2 = ERAS[test][1]
        if datetime.time(t1, 0) < dtobj.time() < datetime.time(t2, 0):
            era = test
    return era


def rand_string(L=8):
    '''Generate random ascii string'''
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(L))


def rand_address():
    '''Generate random street address'''
    num = random.randrange(3, 5000)
    name = rand_string(L=random.randrange(4, 10+1))
    kind = random.choice(['', '', ' road', ' lane', ' building', ' street', ' place', ' avenue', ' blvd'])
    return str(num) + ' ' + name + kind


def random_time():
    '''
    Generate random order time

    Conditions:
    - in 2019
    - between 6:00 and 23:59 local time
    - prior to now
    '''
    otime = datetime.datetime(year=5000, month=1, day=1, tzinfo=datetime.timezone.utc)
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    year = 2019
    tzinfo = datetime.timezone.utc
    while otime > now: # repeat until order is in the past
        try:
            month = random.choice(range(1,12+1)) # Any month
            day = random.choice(range(1,31+1)) # Any date
            hour = random.choice(range(6, 23+1)) # Locally correct time
            minute = random.choice(range(0, 59+1)) # so 600 to 2359
            otime = otime.replace(year=year, month=month, day=day, hour=hour, minute=minute, tzinfo=tzinfo)
        except:
            'Order was in the future. Let\'s try again'
    return otime


'''
User/location generation
'''

def make_users(n, interval=1):
    '''
    generate `n` random users

    interval>0 so that 'creation_time' attribute is different
    '''
    users = []
    errors = []
    for _ in range(n):
        time.sleep(interval)
        success, d = make_user()
        if success:
            users.append(d)
        else:
            errors.append(d)
    return users, errors

def make_user():
    '''generate and upload a random user'''
    url = ROOT_URL + '/api/login/'
    uname = rand_string(L=random.randrange(6, 18+1))
    userdata = {'uname': uname}
    r = requests.post(url, json=userdata)
    if r.ok:
        return True, r.json()['data']
    return False, r.json()['error']




def make_locations(n, Chicago=True):
    '''generate `n` random locations'''
    locations = []
    errors = []
    for _ in range(n):
        success, d = make_location(Chicago=Chicago)
        if success:
            locations.append(d)
        else:
            errors.append(d)
    return locations, errors
    
def make_location(Chicago=True):
    '''generate + upload a random location'''
    url = ROOT_URL + '/api/locations/'
    name = rand_address()
    locdata = {'loc_name': name}
    if not Chicago:
        locdata['lat'] = random.uniform(10, 80)
        locdata['lng'] = random.uniform(-179.5, 179.5)
        # print(locdata)
    r = requests.post(url, json=locdata)
    if r.ok:
        return True, r.json()['data']
    return False, r.json()['error']
 


'''
Data used to introduct "bias" in order generation.
Obviously real users are not equally likely to order all possible meals or from all possible locations.
'''

user_locs = {
    1: [2, 3],
    2: [4, 7],
    3: [7],
    4: [4, 6],
    5: [4, 5],
    6: [1, 3, 4],
    7: [4], 
    8: [4]
}

us_hard_meal = { 
    1: {'Egg McMuffin': 1, 'Hot Caramel Sundae': 1}
}


'''
Meat and bones of order generation
'''

def make_orders_raw(users, locations, n=10):
    '''generate `n` random orders for users selected from `users`, at locations selected from `locations`'''
    orders = []
    for _ in range(n):
        us = random.choice(users)
        # loc = random.choice(locations)
        loc = random.choice(user_locs.get(us, locations))
        orders.append(make_order_raw(us, loc))
    return orders    


def make_order_raw(us=None, loc=None):
    '''Generate a random order json'''
    contents = us_hard_meal.get(us, make_order_content())
    # contents = [make_order_content(), us_hard_meal.get(us, make_order_content())]
    # contents = random.choice(contents)
    price = round(abs(random.gauss(10, 5)), 2)
    order_time = str(random_time())
    # now = datetime.datetime.now(datetime.timezone.utc)
    # time = str(now)
    # era = get_era(now)
    # return {'id': oid, 'uid': us, "loc_id": loc, "contents": json.dumps(contents), "price": price, 'timestamp': order_time}
    return {'uid': us, "loc_id": loc, "contents": contents, "price": price, 'fake_time': order_time}


def make_order_content():
    '''Randomly generate order content'''
    def list_to_dict(ls):
        dc = {}
        for item in ls:
            dc[item] = dc.get(item,0) + 1
        return dc

    TOTAL_QUANTITY = math.ceil(abs(random.gauss(3,2)))
    contents_list = []
    for _ in range(TOTAL_QUANTITY):
        contents_list.append(random.choice(MENU_ITEMS))
    
    contents = list_to_dict(contents_list)
    return contents


def make_upload_orders(n=10, update_suggestions=True):
    '''Make and upload `n` random orders, with option to update meal recommendation database'''
    # orders = repo.download('order')
    uids = repo.get_all_uids()
    loc_ids = repo.get_all_loc_ids()
    # start = repo.nextid(orders)
    print('generating',n,'new orders')
    new_orders = make_orders_raw(users=uids, locations=loc_ids, n=n)
    url = ROOT_URL + '/api/orders/'

    if input('print new orders? y if yes: ')=='y':
        print('New orders:')
        print(pd.DataFrame(new_orders))
    if input('upload? y if yes: ')=='y':
        statuses = []
        for o in new_orders:
            update_now = (update_suggestions and o is new_orders[-1])
            if update_now:
                o['update_suggestions'] = True
            response = requests.post(url, json=o)
            statuses.append(response.ok)
            if update_now:
                if response.json()['success']:
                    if response.json()['data']['updated_meal_suggestions']:
                        new_recommendations = response.json()['data']['meal_suggestion_update_data']
                    else:
                        new_recommendations = 'No new recommendation data.'
                else:
                    new_recommendations = 'Failure to update meal recommendation data.'
        print('success:', statuses)
        if update_suggestions:
            print('New recommendations:')
            print(new_recommendations)
    else:
        print('Never mind then.')


if __name__=='__main__':
    import sys
    N = 1
    for n in sys.argv[1:]:
        try:
            N = int(n)
        except:
            'never mind'
    update = 'update' in sys.argv
    make_upload_orders(N, update_suggestions=update)














'''
I/O
'''

def export_basic_info(info):
    import os
    if not os.path.exists('usual_draft01_hidden_data'):
        os.mkdir('usual_draft01_hidden_data')
    filename = os.getcwd() + '/usual_draft01_hidden_data/fake_data_info_short.txt'
    with open(filename, 'a+') as file:
        file.write(str(info)+'\n\n')
    print(filename)



# def make_completely_fake_fav_meal(n=10, startid=1):
#     '''takes care of downloading and uploading'''
#     try:
#         uids = [int(user.uid) for user in repo.get_all_rows(repo.download('user'))]
#         loc_ids = [int(loc.id) for loc in repo.get_all_rows(repo.download('loc'))]
#         fmeal = repo.download('fmeal')
#     except:
#         return False

#     # new_fmeals = []
#     for i in range(n):
#         od = json.dumps(make_order_content())
#         uid = random.choice(uids)
#         loc_id = random.choice(loc_ids)
#         t = str(random_time().time())
#         data = {'contents': od, 'primary_loc': loc_id, 'time': t, 'weight': round(random.random()**6, 6), 'id': startid+i, 'uid': uid, 'extra': 1}
#         fmeal.append(data, ignore_index=True)
#         # new_fmeals.append(data)
    
#     try:
#         repo.upload('fmeal', fmeal)
#     except:
#         return False

#     return fmeal






