# Setting up your own system with this code

By [Elisabeth](https://www.github.com/esfinkel/) [Finkel](mailto:finkelelisabeth@gmail.com), for use by Publicis Sapient, with advice from Amit Kumar Sharma and Harrison Pearl.

Started 2019-06-19; last updated 2019-08-02.

## Steps

1. Clone files
2. Create an AWS Lambda bucket
    - Configure bucket access; keep track of the bucket name and access keys
3. Create a personal_data.py file
    - Should have:
        - `secrets = {'key': ('AMAZON AWS ACCESS KEY ID'), 'secret_key': ('AMAZON AWS SECRET ACCESS KEY')}`
        - `bucket_name = 'NAME-OF-THE-BUCKET-YOUR-CSV-FILES-WILL-INHABIT'`
        - `GEO_API_USERNAME = 'USERNAME' # for api.geonames.org/timezoneJSON?`
            - You will need to get your own API key (username) [from geonames.org](https://www.geonames.org/login)
                - Keys are free but are limited to about 1000 calls/hour
            - You then enable your account for API usage
4. Configure serverless and use it to deploy app.
    - Include requirements in [requirements.txt](requirements.txt) (you can probably use mine)
    - Make your own serverless.yml; copy mine, where applicable
    - I recommend using Python 3.6 (see next step).
    - I used [this tutorial](https://medium.com/@Twistacz/flask-serverless-api-in-aws-lambda-the-easy-way-a445a8805028)
        - its screenshots are helpful as well
	    - (should be wsgi_handler.handler not wsgi.handler)
5. AWS Lambda doesn't natively have numpy/pandas, and if I listed them in requirements.txt, Lambda wasn't able to read the included version.
    - I didn't find much information online but we theorized that it was an issue with the Python version and/or OS.
    - I tried compiling the troublesome dependencies on a linux machine (w/ Python 3.6) and uploading them to AWS S3, and creating a lambda layer with access to the bucket (the zip file is too large to upload directly to Lambda).
    - This worked, and the lambda app can import the package with normal syntax.
    - You can try using [my zip file](https://bitbucket.org/elifinke/numpy-pandas-linux-py3-6-compilation/src/master/) (configured for Python 3.6), or one of the built-in layers, or make your own.



### How I compiled troublesome dependencies
1. Import dependencies on a linux machine, then export to your computer
	[This tutorial](https://medium.com/i-like-big-data-and-i-cannot-lie/how-to-create-an-aws-lambda-python-3-6-deployment-package-using-docker-d0e847207dd6) uses docker.
2. Put dependencies in a folder entitled "python"
    - tree should be something like:
        - python
            - numpy
            - numpy-1.1.1.dist-info
            - etc...
3. go to parent directory and zip with something like `zip -r python_libs.zip python/`
	- so zip file will have a name like python_libs.zip
4. Upload to s3 with "aws s3 cp python_libs.zip s3://bucket-name/"
	- aws-cli needs to be configured
5. Go to lambda -> layers -> create layer (or create version if you already have a layer)
	- Use url to the s3 file
6. Go to lambda -> functions -> the specific function -> layers -> add a layer
	- And add layer to function
7. You're good to go! Remember not to list those dependencies in the requirements.txt
8. Deploy lambda functions normally. serverless.yml must specify python 3.6 to align with the docker python version 