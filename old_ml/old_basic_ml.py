import sys
import datetime
import json
import math, statistics
import pandas as pd
import numpy as np

import repo  # Has functions: extract_date, extract_time_from_object, get_all_fav_meals_pd,

                            # get_all_orders_pd, nextid, get_all_uids, getrows, s_fmeal, s_order, upload


import warnings


# Popularity-based recommendations

######################
# Hardcoded values
threshold = 0.2
mincount = 4
#####################



def important_enough_set(sim_orders, orders):
    '''
    "Is this order significant enough, relative to the whole order history, to be recommended?"
    This is the current algorithm.
    
    `sim_orders` is a list of similar orders (series)
    `orders` is the list of all orders (list of series)    
    '''
    count = len(sim_orders)
    weight = count / len(orders)

    return (weight > threshold or count >= mincount), weight


def orders_like_this_order(order, orders):
    '''
    `order` is a user's order (series)
    `orders` is the user's order history (list of series)
    
    Return a list of all orders that match the order (including order itself)
    Each matching order is a series
    '''
    matches = filter(
        (lambda order_x: contents(order_x)==contents(order) and order_x['loc_id']==order['loc_id']),
        orders
    )
    return list(matches)


def contents(order):
    return json.loads(order['contents'])
    

def get_time_range(matches):
    '''
    Given a list of similar orders (list of series),
    What is the range of order times? Return two datetime.time objects
    '''
    first = None
    last = None
    for order in matches:
        o_time = repo.extract_time_from_object(order['timestamp'])
        if first is None or o_time < first:
            first = o_time
        if last is None or last < o_time:
            last = o_time
    return first, last


    # mid == meal_id
def make_fav_meal_raw(orders, mid, weight=None):
    '''
    orders is a list of series
    helper function to generate a 'meal recommendation' table entry based on order `order`
    '''
    if len(orders)==0:
        return None
    order = orders[0]
    first_time, last_time = get_time_range(orders)
    # Schema: 'id' (int), 'uid' user's id (int), 'contents' (str(dict)), 'time' avg order time (str), 'primary_loc' location id (int) 
    meal = {'id': mid,
            'contents': contents(order),
            'first_time': str(first_time),
            'last_time': str(last_time),
            'weight': weight
        }
    return meal


def verify(doing_what, printif=None, printelse=None):
    # For i/o purposes
    print('Do you want to ' + doing_what + '?')
    truth = input("'y' if yes; ENTER otherwise: ")=='y'
    if printif is not None and truth:
        print(printif)
    if printelse is not None and not truth:
        print(printelse)
    return truth


def generate_popular_orders(quiet=False):
    """
    - Download current order history,
    - determine whether any meals should be recommended,
    - and then return recommendation list.
    """
    orders = repo.get_all_orders()
    if not quiet:
        print('Found '+str(len(orders))+' total orders.')
    nid = 1
    new_favs = []
    ignore = set()
    if not quiet:
        max_num_dits = 10
        num_dits = 0
        print('Progress:')
        sys.stdout.write("[%s]" % (" " * max_num_dits))
        sys.stdout.flush()
        sys.stdout.write("\b" * (max_num_dits+1)) # return to start of line, after '['
    for order in orders:
        if not quiet and (order['id']*max_num_dits)//len(orders) > num_dits:
            sys.stdout.write("-")
            sys.stdout.flush()
            num_dits += 1
        if order['id'] not in ignore:
            similar_meals = orders_like_this_order(order, orders)
            ignore = ignore.union(set([m['id'] for m in similar_meals]))
            importance, weight = important_enough_set(similar_meals, orders)
            if importance:
                new_order = make_fav_meal_raw(similar_meals, nid, weight)
                new_favs.append(new_order)
                nid += 1
    if not quiet:
        sys.stdout.write("]\n") # this ends the progress bar
        sys.stdout.flush()
    return deal_with_info(new_favs, quiet=quiet)


# with make_holistic_order_user_pair_csv_from_web
# I could modify it to work with make_order_item_pair_csv_from_web
def generate_user_based_reccs(df=None, infile=None, download=False, uid=None, quiet=False):
    assert (uid is not None), 'You need to provide a uid'
    assert (df is not None) + (infile is not None) + (download)==1, 'You need to provide either a df or a csv or download instructions'
    if infile is not None and df is None:
        df = pd.read_csv(infile)
    if download:
        import some_data_pro
        df = some_data_pro.make_holistic_order_user_pairs_from_web()
    # totals = df.groupby('contents')['times_ordered'].sum()
    # most_ordered_things = df.groupby('contents')['times_ordered'].sum().sort_values(ascending=False).head()
    crosstab = pd.pivot_table(data=df, values='times_ordered', index='uid', columns='contents')
    user_orders = df.query('uid==%s'%uid)
    user_tops = user_orders.sort_values(by='times_ordered', ascending=False).head()
    user_corr = None
    import warnings
    for _, row in user_tops.iterrows(): # _ is index
        order_quant = crosstab[row['contents']] # .dropna()
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')
            similar_to_order = crosstab.corrwith(order_quant) * row['times_ordered']        
        if user_corr is None:
            user_corr = similar_to_order
        else:
            user_corr = user_corr.add(similar_to_order, fill_value=0) #, on='contents')
    return user_corr.sort_values(ascending=False).head().index.tolist()


def deal_with_info(favs, quiet):
    if len(favs)==0 and not quiet:
        print('No suggestions to recommend.')
    if len(favs)>0 and not quiet:
        verify('print meal suggestions', 'suggestions:\n'+str(favs))
    return favs


def experiment_print():
    import time
    toolbar_width = 40
    sys.stdout.write("[%s]" % (" " * toolbar_width))
    sys.stdout.flush()
    sys.stdout.write("\b" * (toolbar_width+1)) # return to start of line, after '['

    for _ in range(toolbar_width):
        time.sleep(1) # do real work here
        # update the bar
        sys.stdout.write("-")
        sys.stdout.flush()

    sys.stdout.write("]\n") # this ends the progress bar


if __name__=='__main__':
    # generate_popular_orders(quiet=False)
    # experiment_print()
    print(generate_user_based_reccs(infile='holistic_order_user_pairs.csv', uid=6))


