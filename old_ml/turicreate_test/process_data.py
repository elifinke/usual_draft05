import requests, json
import math, random
import pandas as pd
import numpy as np
from parse import search
import sys, os



"""
Python 3.6 only!
"""
# def verify_p():
v = sys.version_info[0:2] 
# print(v)
if v > (3, 6):
    raise Exception("Turicreate does not yet have support for Python beyond 3.6.")
elif v < (3, 5):
    raise Exception("Python 3.5 or 3.6 is required.")
else:
    import turicreate as tc



# verify_p()

'''
i/o
'''

class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout


'''
Core data processing
'''


def import_menu_items():
    try:
        from hardcoded_res import MENU_ITEMS
    except:
        MENU_ITEMS = ['UNKNOWN'] * 199
    return MENU_ITEMS

def import_classification():
    try:
        from hardcoded_res import classify_meal_photo_full as CLASSIFY_MEAL
    except:
        CLASSIFY_MEAL = lambda x: 'FD'
    return CLASSIFY_MEAL

def hour_min(timestring):
    hour, min = search(' {:d}:{:d}', timestring)
    tz_finds = search('+{:2}:{:2}', timestring)
    if tz_finds is None:
        tz_finds = ('00', '00')
    tz = '+'+tz_finds[0]+':'+tz_finds[1]   
    # tz = ':'.join(tz_finds) # tz_finds[0]+':'+tz_finds[1]   
    return round(hour+min/60, 2), tz   

def time_obj(timestring):
    return search('{:ti}', timestring)[0]

def format_content_list_rating_system(orders):
    ITEMS = import_menu_items()
    classify = import_classification()
    order_item_table = []
    for o in orders:
        for i in o['contents'].keys():
            entry = {'food_id': ITEMS.index(i), 'quantity': int(o['contents'][i]), 'uid': int(o['uid']), 'loc': int(o['loc_id'])}
            entry['category'] = classify({i: None})
            entry['food_name'] = i
            entry['hour_min'], entry['tz'] = hour_min(o['timestamp'])
            entry['time_obj'] = time_obj(o['timestamp'])
            # print({i: None}, classify({i: None}))
            order_item_table.append(entry)
    return order_item_table


def download_format_make_csv(filepath='order_item_table.csv'):
    url = 'https://x9gctr0aac.execute-api.us-east-1.amazonaws.com/dev/api/orders/all/'
    orders = json.loads(requests.get(url).content)
    orders = orders['data']

    order_item_table = format_content_list_rating_system(orders)

    with open(filepath, 'w') as file:
        file.write(pd.DataFrame(order_item_table).to_csv(index=False))


'''
Core ML
'''


def make_model(filepath='order_item_pairs_2.csv'):
    items = tc.SFrame.read_csv(filepath, verbose=False)
    training_data, validation_data = tc.recommender.util.random_split_by_user(items, user_id='uid', item_id='food_id')
    with HiddenPrints():  # verbose=False doesn't remove model prints. idk why
        model = tc.recommender.create(observation_data=training_data, user_id='uid', item_id='food_id', target='quantity', item_data=items, verbose=False)
    return model, validation_data


def recc_item_for_user(model, uid, N=3):
    '''model is a turicreate model, uid is an int/string, N is an int'''
    users = [int(uid)]
    recommendations_data = model.recommend(users=users, exclude_known=False, exclude=None, verbose=False, diversity=1.5, k=N)
    reccs = recommendations_data['food_id'].to_numpy()
    # reccs = list(recommendations_data.to_dataframe().food_id)[:N]
    menu = import_menu_items()
    recc_items = np.array(menu)[reccs]
    return list(reccs), list(recc_items)


# def recc_ion(model):
#     returm model.recommend_from_interactions(model)

def okay_meal(mealtype):
    if mealtype is '': return False
    if len(mealtype)==1: return True
    return set(mealtype)==set('FD') # and abs(mealtype.count('D')-mealtype.count('F'))<=2

def list_to_dict(ls):
    dc = {}
    for item in ls:
        dc[item] = dc.get(item,0) + 1
    return dc


def predict_from_file(arg_string):
    user = search('-u {:d}', arg_string)
    if user is None:
        return print('Specify a uid with -u <int>')
    else:
        user = user[0]

    N = search('-n {:d}', arg_string.lower())
    if N is None:
        print('Random number of items')
        N = int(math.ceil(abs(random.gauss(3, 2))))
    else:
        N = N[0]

    model_info = search('-m {:1}', arg_string)
    if model_info is None or model_info[0] not in ['o', 'n']:
        return print('Specify model with -m o/n')
    if model_info[0]=='o':
        # old model
        try:
            model = tc.load_model('my_model')
        except:
            return print('Could not find old model.')
    else: # model_info[0]=='n':
        model = make_model()[0]
        model.save('my_model')

    mealtype = ''
    classify = import_classification()
    i, TRIES = 0, 1
    while not okay_meal(mealtype) and i < TRIES:
        # print('try',i+1)
        recc = recc_item_for_user(model, user, N)
        mealtype = classify(recc[1])
        i += 1
    recc = recc_item_for_user(model, user, N)
    print(recc[0])
    print(list_to_dict(recc[1]))

if __name__ == "__main__":
    from sys import argv
    if 'download' in argv:
        print('You can recreate source data by running some_data_pro.py (in the version folder).')
    else:
        predict_from_file(arg_string=' '.join(argv[1:]))
















# def recc_item(model, uids=None, times=None, N=3):
#     # Unfinished bc I don't really understand it
#     criteria = {}
#     if uids is not None:
#         if not isinstance(uids, list):
#             uids = [uids]
#         criteria['uid'] = uids
#     if times is not None:
#         if not isinstance(times, list):
#             times = [times]
#         criteria['hour_min'] = times
#     query = tc.SFrame(criteria)
#     recommendations_data = model.recommend(users=query, exclude_known=False)
#     reccs = list(recommendations_data.to_dataframe().food_id)[:N]
#     recc_items = np.array(import_menu_items())[reccs]
#     return reccs, recc_items    