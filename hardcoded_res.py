#!/usr/bin/env python3.7
'''
Hardcoded resources. Mostly menu-related.
'''


import random

import personal_data


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-06-24"
__updated__ = "2019-08-01"

'''
Constants 
'''


SECONDS_PER_TWO_HOURS = 60*60*2

SECONDS_PER_THIRTY_DAYS = 60*60*24*30

DEFAULT_LOCATION = (41.886, -87.629)

LOCATION_DELTA = (0.05, 0.01)

# MIN_WITHIN = 120

LOCATION_MARGIN_OF_ERROR = 0.00001

STDEV_CUTOFF = 1.5

SUPERUSER_UID = 10
SUPERUSER_MEAL = 20 # fmeal id   # bc favmeal with id 20 has lots of menu items


# GEO_API_USERNAME is functionally an api key for http://api.geonames.org/
def GEO_API_ACCESS_INFO():
    return 'username='+personal_data.GEO_API_USERNAME



'''
Generate locations
'''

def MAKE_LATLNG():
    '''Generate random location in the Chicago area. Might be in Lake Michigan.'''
    lat = DEFAULT_LOCATION[0] + LOCATION_DELTA[0]*random.choice([1,-1])*random.random()
    lng = DEFAULT_LOCATION[1] + LOCATION_DELTA[1]*random.choice([1,-1])*random.random()
    return lat, lng


'''
Create menu
'''

def add_sizes(raw_items, possible_sizes):
    items = []
    for item in raw_items:
        items += [size+' '+item for size in possible_sizes]
    return items


drinks_raw = ['Coca-Cola', 'Sprite', 'Fanta Orange', 'Dr Pepper', 'Diet Coke',
        'Chocolate Shake', 'Vanilla Shake', 'Strawberry Shake', 'Hot Chocolate',
        'Iced Tea', 'Sweet Tea', 'Chocolate Milk', '1% Low Fat Milk', 'DASANI Water',
        'Minute Maid Premium Orange Juice', 'Minute Maid Fruit Punch Slushie',
        'Honest Kids Appley Ever After Organic Juice Drink', 'Minute Maid Blue Raspberry Slushie',
        'Minute Maid Sweet Peach Slushie']

drinks = add_sizes(drinks_raw, ['Small', 'Medium', 'Large'])

foods1_raw = ['Egg McMuffin', 'Sausage McMuffin', 'Bacon, Egg & Cheese Biscuit', 'Sausage Biscuit',
        'Bacon, Egg & Cheese McGriddles', 'Sausage McGriddles', 'Hotcakes', 'Fruit \'N Yogurt Parfait',
        'Hash Browns', 'Quarter Pounder with Cheese Bacon', 'Quarter Pounder with Cheese', 'Big Mac',
        'Big Mac', 'Big Mac', 'Double Cheeseburger', 'Cheeseburger', 'Hamburger',
        '4 piece Chicken McNuggets', 'Buttermilk Crispy Tenders', 'Artisan Grilled Chicken Sandwich',
        'Buttermilk Crispy Chicken Sandwich', 'McChicken', 'Filet-O-Fish',
        'Bacon Ranch Salad with Buttermilk Crispy Chicken', 'Bacon Ranch Grilled Chicken Salad',
        'Southwest Buttermilk Crispy Chicken Salad', 'Southwest Grilled Chicken Salad']


# foods1 = add_sizes(foods1_raw, ['X'])
foods1 = foods1_raw[:]

foods2_raw = ['Hot Caramel Sundae', 'Strawberry Sundae', 'McFlurry with OREO Cookies', 'Baked Apple Pie',
        'Chocolate Chip Cookie', 'Hamburger Happy Meal', '4 piece Chicken McNuggets Happy Meal',
        '6 piece Chicken McNuggets Happy Meal', 'Cheesy Bacon Fries from Australia', 'World Famous Fries',
        'Apple Slices', 'Yoplait GO-GURT Low Fat Strawberry Yogurt', 'Side Salad']

foods2 = foods2_raw[:]

MENU_ITEMS = drinks + foods1 + foods2


def classify_meal_photo(mealdict):
    '''Does this meal have a food, a drink, or both?'''
    result = set()
    for item in mealdict.keys():
        if mealdict[item]==0:
            continue
        if item in drinks:
            result.add('D')
        if item in foods1+foods2:
            result.add('F')
    return ''.join(result)

