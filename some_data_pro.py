#!/usr/bin/env python3.7
'''
i.e. some_data_processing. Functions to download order data, analyze it, reformat it a certain way, and save as a CSV.

Might be used in testing future ML algorithms
'''


# ie some_data_processing

import json
import pandas as pd

import repo
from hardcoded_res import MENU_ITEMS


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-07-18"
__updated__ = "2019-08-01"


def download_user_olist(uid):
    '''Retrieve all orders by user with uid `uid`; return json serialization'''
    x = repo.download('order')
    orders = [json.loads(o) for o in x.query('uid==%s'%uid)['contents'].to_list()]
    return orders


def itemcount(orderslist, partial_food_name):
    '''Given serialized order list, print how many times food with `partial_food_name` has been ordered'''
    count = 0
    found = {}
    for o in orderslist:
        for k,v in o.items():
            if partial_food_name.lower() in k.lower():
                found[k] = found.get(k, 0) + v
                count += v
    print('Found:\n'+str(found))
    print('Total count: '+str(count))


def top_items(orderslist, N=None, mincount=None):
    '''
    Given serialized order list, find most-often-ordered items
    Either top `N` items or all items ordered more than `mincount` times
    '''
    counts = {}
    for o in orderslist:
        for k,v in o.items():
            counts[k] = counts.get(k, 0) + int(v)
    tops = sorted(list(counts.items()), key = (lambda f: counts[f[0]]), reverse=True)
    # Mode: filter by mincount
    if isinstance(mincount, int) and mincount>0:
        return list(filter(lambda p: p[1]>=mincount, tops))
    # Mode: list certain number
    if isinstance(N, int) and N>0:
        N = min(N, len(tops))
    else:
        N = len(tops)
    return tops[:N]





def make_order_item_total_df_from_scratch():
    '''Download data and format into special df'''
    orders = repo.download('order')
    order_item_total_table = {}
    users = [int(u) for u in orders.uid.unique()]
    for u in users:
        order_item_total_table[u] = {}
        u_orders = [json.loads(o) for o in orders.query('uid==%s'%u)['contents'].to_list()]
        for o in u_orders:
            for food, quan in o.items():
                order_item_total_table[u][food] = order_item_total_table[u].get(food, 0) + int(quan)
    df = pd.DataFrame(order_item_total_table).fillna(0).transpose() # .set_index(users)
    df.index = df.index.rename('uid')
    for col in df.columns:
        df[col] = df[col].astype(int)
    return df

def make_order_item_pair_df_from_oitdf(oitdf):
    '''Convert one special type of df to another'''
    newlist = []
    users = None
    for o in oitdf.iteritems():
        food, quans = o
        if users is None:
            users = list(quans.index.unique())
        for u in users:
            fquan = int(quans[u])
            food_id = MENU_ITEMS.index(food)
            newlist.append((u, food_id, fquan))
    oipdf = pd.DataFrame(newlist, columns=['uid', 'food_id', 'quantity'])
    return oipdf

        


def make_order_item_pair_csv_from_web(outputfile='example.csv'):
    '''Download data and make specially-formatted csv'''
    df = make_order_item_total_df_from_scratch()
    pair_df = make_order_item_pair_df_from_oitdf(df)
    with open(outputfile, 'w') as f:
        f.write(pair_df.to_csv())



def make_holistic_order_user_pair_csv_from_web(outfile='holistic_order_user_pairs.csv'):
    '''Data formatting'''
    df = make_holistic_order_user_pairs_from_web()
    with open(outfile, 'w') as f:
        f.write(df.to_csv())

def make_holistic_order_user_pairs_from_web():
    '''Data formatting'''
    orders = repo.download('order')
    orders_w_relevant_users = orders.groupby('contents')['uid'].apply(list) # indices: orders; entries: list of relevant uids
    res1 = orders_w_relevant_users
    res2 = []
    for c in res1.index.to_list():
        for u in set(res1[c]):
            res2.append({'contents': c, 'uid': u, 'times_ordered': res1[c].count(u)})
    df = pd.DataFrame(res2)
    return df



if __name__ == "__main__":
    from sys import argv
    def err():
        print('Unable to process')
        print('Make sure you provide an outfile to write/overwrite as the argv')
    if len(argv)>1:
        if 'holistic' in argv:
            make_holistic_order_user_pair_csv_from_web()
        else:
            try:
                outfile = argv[1]
                assert len(outfile)>1
                assert '.csv' in outfile
                make_order_item_pair_csv_from_web(outfile)
            except:
                err()
    else:
        err()
