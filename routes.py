#!/usr/bin/env python3.7
'''
Route handling and some business logic.

Uses functions from repo.py as helper methods.

Agnostic as to the implementation of repo.py.

Elisabeth Finkel; created 2019-06-19; modified 2019-06-20 (Header dates in CST. Business logic time in UTC.)
https://5tvw5wvm2l.execute-api.us-east-1.amazonaws.com/dev/api/options.html
'''

"""
Status codes:
200 - OK        GET -resource
https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
"""

from flask import request, redirect, jsonify, Flask
import json
import datetime, math, random
from parse import search

import repo
from repo import s_user, s_order, s_loc, s_fmeal
import hardcoded_res
import basic_ml


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-06-19"
__updated__ = "2019-08-01"

"""
Instantiate app
"""
app = Flask(__name__)   # I'm using Flask to handle routing just because I'm used to it.
                        # Transitioning to something else doesn't look to intensive.


"""
Internal i/o helper methods
"""

def gets(kw,arg):
    '''Extract string'''
    return kw.get(arg,'')

def geti(kw,arg,default=-1):
    '''Extract integer'''
    try:
        return int(kw.get(arg,default))
    except:
        return default

def getf(kw,arg):
    '''Extract float'''
    try:
        return float(kw.get(arg, math.inf))
    except:
        return math.inf

def getdict(kw, arg):
    '''Extract dictionary'''
    d = kw.get(arg, {})
    if isinstance(d, str):
        try:
            d = json.loads(d)
        except:
            d = {}
    return d

# json.loads(request.data)   # request body. handles 'POST', 'PUT' and 'PATCH'
# req.args               # "params" (not header?). the part after the question mark?

options_list = []

def options_table():
    '''Returns list of options'''
    options_list.sort()
    return {'Options': options_list}


def missing(**kwargs):
    '''Helper to handle missing fields'''
    fields = gets(kwargs, 'field') + gets(kwargs, 'fields')
    mis = 'Missing (or invalid) required '
    if len(fields) == 0:
        mis += 'fields'
    else:
        if len(fields.split(','))>1:
            mis += 'fields: '
        else:
            mis += 'field: '
        mis += fields
    return jsonify({'success': False, 'error': mis}), 400


def invalid(**kwargs):
    '''Helper to handle invalid fields'''
    fields = gets(kwargs, 'field') + gets(kwargs, 'fields')
    inv = 'Invalid '
    if len(fields) == 0:
        inv += 'fields'
    else:
        if len(fields.split(','))>1:
            inv += 'fields: '
        else:
            inv += 'field: '
        inv += fields
    return jsonify({'success': False, 'error': inv}), 400


def could_not_retrieve(field=None):
    '''Helper to handle data retrieval issues'''
    cnr = 'could not retrieve data'
    if field is not None:
        cnr += ': ' + field
    return jsonify({'success': False, 'error': cnr}), 502



"""
Other internal helper methods
"""

def current_utc_dtime():
    '''Return timezone-aware datetime object in UTC'''
    return datetime.datetime.now(tz=datetime.timezone.utc)


def valid_contents_dict(d):
    '''Return True if every menu item in the dict was ordered with quantity int>0'''
    def valid_num(v):
        return isinstance(v, int) and v>0 
    def valid_item(i):
        return i.lower() in [j.lower() for j in hardcoded_res.MENU_ITEMS]
    quantities_ok = all([valid_num(v) for v in d.values()])
    items_ok = all([valid_item(i) for i in d.keys()])
    return items_ok and quantities_ok



"""
Various endpoints that exist mostly for dev use
"""
@app.route('/')
def redir():
    return redirect(request.url_root+'api', code=302)


options_list.append('/api/')
@app.route('/api/', methods=['GET'])
def main_menu():
    '''Landing page'''
    return jsonify({'success': True, 'data': 'Welcome!'}), 200 #, 'headers': {'Content-type': 'application/json'}}), 200


options_list.append('/api/options/')
@app.route('/api/options/')
def options():
    '''Options json'''
    return jsonify({'success': True, 'data': options_table()}), 200


options_list.append('/api/options.html')
@app.route('/api/options.html')
def options_html():
    '''html-formatted options'''
    options = ('<html><head><title>Options</title></head>' +
    '<body style="font-size:20px"><ul><li>' +
    '</li><li>'.join("<a href='"+request.url_root[:-1]+opt.split()[0]+"'>"+opt+"</a>" for opt in options_table()['Options']) +
    '</li></ul></body></html>')
    return options, 200


@app.route('/api/users/super/')
def get_super_user():
    '''Super user always has a hardcoded meal recommended for this. this retrieves that data.'''
    suid = hardcoded_res.SUPERUSER_UID
    smeal = hardcoded_res.SUPERUSER_MEAL
    return jsonify({'success': True, 'data': {'uid': suid, 'fmeal_id': smeal}})


@app.route('/cache/all/')
def store_data_copies_on_s3():
    '''Cache all databases in S3'''
    data = repo.store_data_copies_on_s3()
    return jsonify({'success': True, 'data': data}), 200



"""
API endpoints
"""

options_list.append('/api/login/ (POST)')
@app.route('/api/login/', methods=['POST'])
def add_user():
    '''Make user obj with appropriate username and creation time'''
    user_info = json.loads(request.data)
    uname = user_info.get('uname', '').strip()
    if uname=='':
        return missing(field='uname')
    dtime = current_utc_dtime()
    success, new_user = repo.make_new_user(uname, dtime)
    if success:
        return jsonify({'success': True, 'data': s_user(new_user)}), 201
    else:
        return e400()


options_list.append('/api/users/all/')
@app.route('/api/users/all/')
def get_all_users():
    '''Retrieve list of all users'''
    users = repo.get_all_users()
    if users is None:
        return could_not_retrieve('user data')
    users_ser = [s_user(us) for us in users]
    return jsonify({'success': True, 'data': users_ser}), 200


options_list.append('/api/login/')
@app.route('/api/login/', methods=['GET'])
def check_user():
    '''Check to see if a user exists, and if so, return user information.'''
    user_info = request.args
    uid = geti(user_info, 'uid')
    if uid<=0:
        return missing(field='uid')
    user = repo.find_user(uid)
    if user is None:
        return jsonify({'success': True, 'data': 'Could not find user'}), 200
    return jsonify({'success': True, 'data': s_user(user)}), 200
    # return json.dumps({'success': True, 'data': user}), 200


options_list.append('/api/users/all-uids/')
@app.route('/api/users/all-uids/')
def all_uids():
    '''Retrieve list of all uids'''
    uids = repo.get_all_uids()
    if uids is None:
        return could_not_retrieve('user data')
    # uids = [s_user(user).get('uid') for user in users]
    return jsonify({
        'success': True, 'data': uids
    }), 200



options_list.append('/api/locations/ (POST)')
@app.route('/api/locations/', methods=['POST'])
def make_location():
    '''Register McD location (ie make location obj) given coordinates and name'''
    loc_info = json.loads(request.data)
    loc_name = loc_info.get('loc_name', '')
    lat, lng = getf(loc_info, 'lat'), getf(loc_info, 'lng')
    latlng_given = True
    if lat is math.inf or lng is math.inf or not (abs(lat)<=90 and abs(lng)<=180):
        lat, lng = hardcoded_res.MAKE_LATLNG()
        latlng_given = False
    success, loc = repo.make_location(lat, lng, loc_name)
    if success:
        loc_ser = s_loc(loc)
        loc_ser['randomly_generated'] = not latlng_given
        return jsonify({'success': True, 'data': loc_ser}), 201
    else:
        return e400()


options_list.append('/api/locations/')
@app.route('/api/locations/')
def get_locations():
    '''Retrieve list of registered McD locations'''
    locs = repo.get_locations()
    if locs is None:
        return could_not_retrieve('location data')
    locs_ser = [s_loc(loc) for loc in locs]
    return jsonify({'success': True, 'data': locs_ser}), 200



options_list.append('/api/orders/ (POST)')
@app.route('/api/orders/', methods=['POST'])
def make_order():
    '''Add order to order history database'''
    order_info = json.loads(request.data)

    uid = geti(order_info, 'uid')
    if uid<0:
        return missing(field='uid')

    contents = getdict(order_info, 'contents')
    if contents=={}:
        return missing(field='contents')
    if not valid_contents_dict(contents):
        return invalid(field='contents')

    price = getf(order_info, 'price')
    if price is math.inf:
        return missing(field='price')

    loc_id = geti(order_info, 'loc_id')
    if loc_id<0:
        return missing(field='loc_id (location id)')
    loc = repo.get_loc(loc_id)
    if loc is None:
        return invalid(field='loc_id')
    
    dtime = None
    
    fake_time = order_info.get('fake_time')
    if fake_time is not None:
        fake_time = repo.extract_date(fake_time)
        lat, lng, utc_offset = loc['lat'], loc['lng'], json.loads(loc['utc_offset'])
        dtime = fake_time.replace(tzinfo=datetime.datetime.strptime(utc_offset, '%z').tzinfo)
    if dtime is None:
        time_zone_time = order_info.get('TZtime', True)
        if time_zone_time in [True, 'True', 'true']:
            lat, lng = loc['lat'], loc['lng']
            dtime = repo.gdtime_from_lat_lng(lat, lng)
        else:
            dtime = current_utc_dtime()

    success, new_order = repo.make_order(uid, contents, price, loc_id, dtime)
    if not success:
        return jsonify({'success': False, 'data': 'Error. '+new_order}), 400
    data = {'data': s_order(new_order)}
    if order_info.get('update_suggestions', False) in [True, 'True', 'true']:
        fmeal_creation_success, fmeal_creation_data = basic_ml.download_update_upload_fav_meals(quiet=True)
        if fmeal_creation_data is None or (not fmeal_creation_success):
            data['updated_meal_suggestions'] = False
        else:
            data['updated_meal_suggestions'] = True
            new_favs, changed_faves, manual_collisions = fmeal_creation_data
            data['meal_suggestion_update_data'] = {
                'new_suggestions': new_favs,
                'changed_suggestions': changed_faves,
                'manual_collisions': manual_collisions
            }
    return jsonify({'success': True, 'data': data}), 201


options_list.append('/api/orders/')
@app.route('/api/orders/', methods=['GET'])
def get_order():
    '''Retrieve a particular order from order history'''
    order_info = request.args
    oid = geti(order_info, 'id')
    if oid<=0:
        return missing(field='id')
    order = repo.get_order(oid)
    if order is None:
        return jsonify({'success': True, 'data': 'Order does not yet exist'}), 200
    return jsonify({'success': True, 'data': s_order(order)}), 200
    # return json.dumps({'success': True, 'data': order}), 200


options_list.append('/api/orders/all/')
@app.route('/api/orders/all/', methods=['GET'])
def get_all_orders():
    '''Retrieve entire order history'''
    orders = repo.get_all_orders()
    if orders is None:
        return could_not_retrieve('order data')
    s_orders = [s_order(order) for order in orders]
    return jsonify({'success': True, 'data': s_orders}), 200
    # return json.dumps({'success': True, 'data': orders}), 200



options_list.append('/api/favmeal/ (POST)')
@app.route('/api/favmeal/', methods=['POST'])
def push_fmeal_to_cloud():
    '''
    Add meal recommendation to meal recommendation database given data
    (note that basic_ml directly accesses/modifies database, so any changes to database access must be made there as well)
    '''
    meal_info = json.loads(request.data)

    uid = geti(meal_info, 'uid')
    if uid<0:
        return missing(field='uid')

    contents = getdict(meal_info, 'contents')
    if contents=={}:
        return missing(field='contents')
    if not valid_contents_dict(contents):
        return invalid(field='contents')

    weight = getf(meal_info, 'weight')
    if weight is math.inf:
        return missing(field='weight')

    loc = geti(meal_info, 'loc')
    if loc<0:
        return missing(field='loc (location id)')
    
    time = meal_info.get('time')
    stdev_time = meal_info.get('stdev_time')
    if time is None:
        return missing(field='time')
    fmeal, success = repo.push_fav_meal_to_cloud(uid, contents, weight, loc, time, stdev_time)

    if success is False:
        return jsonify({'success': False, 'data': 'Error. '+fmeal}), 400
    return jsonify({'success': True, 'data': s_fmeal(fmeal)}), 201


def clean_meal(meal):
    '''
    Helper function to add more data to meal recommendations
    '''
    try:
        meal['loc_name'] = repo.get_loc(meal.get('primary_loc')).get('loc_name')
    except:
        meal['loc_name'] = ''
    meal['photo_catogory'] = hardcoded_res.classify_meal_photo(meal.get('contents'))
    meal['price'] = round(abs(random.gauss(10, 5)), 2)
    return meal

options_list.append('/api/favmeal/meal/')
@app.route('/api/favmeal/meal/')
def get_user_loc_top_meal():
    '''
    Given user and location, retrieve top meal recommendation, if any
    '''
    from hardcoded_res import SUPERUSER_MEAL, SUPERUSER_UID
    info = request.args

    uid = geti(info, 'uid')
    user = repo.find_user(uid)
    if user is None:
        return missing(field='uid')
    if uid==SUPERUSER_UID:
        f_meal = repo.get_fmeal(SUPERUSER_MEAL)
        if f_meal is None:
            return could_not_retrieve('meal recommendation data')
        meal = clean_meal(s_fmeal(f_meal))
        data = {'suggest': True, 'meal': meal}
        return jsonify({'success': True, 'data': data}), 200

    gps = geti(info, 'gps')
    if gps not in [1,0]:
        return missing(field='gps')
    if gps:
        lat, lng = getf(info, 'lat'), getf(info, 'lng')
        if lat is math.inf or lng is math.inf:
            return missing(field='the gps coordinates')
        loc_id = repo.loc_id_from_lat_lng(lat, lng)
        # loc_id = loc['id']
        if loc_id is None:
            return invalid(field='lat and/or lng')
    else:
        loc_id = geti(info, 'loc_id')
        loc = repo.get_loc(loc_id)
        if loc is None:
            return missing(field='loc_id')
    
    meals = repo.get_user_loc_fav_meals(uid, loc_id)

    if meals is None:
        return could_not_retrieve('meal recommendation data')

    if len(meals)==0:
        data = {'suggest': False, 'meal': None}
    else:
        meal = clean_meal(s_fmeal(meals[0]))
        data = {'suggest': True, 'meal': meal}

    return jsonify({'success': True, 'data': data}), 200


@app.route('/favmeal/users/')
def get_all_uids_with_meal_recommended():
    '''
    Given location, retrieve all users with a meal recommendation (for testing purposes)
    '''
    uids = repo.get_all_uids()
    if uids is None:
        return could_not_retrieve('user data')
    # uids = [s_user(user)['uid'] for user in users]
    relevant_users = []

    info = request.args
    gps = geti(info, 'gps')
    if gps not in [1,0]:
        return missing(field='gps')
    if gps:
        lat, lng = getf(info, 'lat'), getf(info, 'lng')
        if lat is math.inf or lng is math.inf:
            return missing(field='the coordinates')
        loc_id = repo.loc_id_from_lat_lng(lat, lng)
        # loc_id = loc['id']
        if loc_id is None:
            return invalid(field='lat and/or lng')
    else:
        loc_id = geti(info, 'loc_id')
        loc = repo.get_loc(loc_id)
        if loc is None:
            return missing(field='loc_id')
    
    relevant_users = repo.get_user_loc_fav_meals(uid=None, loc_id=loc_id)
    return jsonify({'success': True, 'data': relevant_users}), 200




"""
Handle errors
"""

def e400():
    return jsonify({'success': False, 'error':'Bad request. May be a syntax or precondition issue. Please double check the specifications.'}), 400

@app.errorhandler(404)
def page_not_found(e):
    return e404()

def e404():
    return jsonify({'success': False, 'error': {'error': '404 Error.', 'options': options_table()}}), 404

@app.errorhandler(500)
def e500(e):
    return jsonify({'success': False, 'error': {
        'error': '500 (Internal server) error. Please try again later?', 'details': repr(e)
        }}), 500









# """
# Endpoints for testing
# """

# options_list.append('/api/dummy/')
# @app.route('/api/dummy/', methods=['GET'])
# def dummy():
#     return jsonify({'success': True, 'data': ['DUMMY DATA']}), 200

# @app.route('/redirect/google/')
# def redirect_google():
#     return redirect("https://www.google.com", code=302)


# @app.route('/api/users/allinfo/')
# def defunct():
#     err = 'Test endpoint has been removed. Try \'/api/options\' to see all available options.'
#     return jsonify({'success': True, 'data': err}), 200

# @app.route('/api/sysinfo/')
# @app.route('/sysinfo/')
# def sys_info():
#     '''used for debugging numpy/pandas config stuff'''
#     req = request.args
#     import sys
#     info = {'Python version': sys.version}
#     if req.get('numpy',False) in [True, 'True', 'true']:
#         import numpy
#         info['Numpy version'] = numpy.__version__
#     if req.get('pandas',False) in [True, 'True', 'true']:
#         import pandas
#         info['Pandas version'] = pandas.__version__
#     return jsonify({'success': True, 'data': info}), 200
