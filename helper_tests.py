#!/usr/bin/env python3.7
'''
Can be used to hardcode an order and then test its recommendation time.
'''


import repo
import datetime, json


__author__ = "Elisabeth Finkel"
__copyright__ = "For use by Publicis Sapient"
__credits__ = ["Elisabeth Finkel", "Amit Kumar Sharma", "Harrison Pearl"]
__license__ = "TBD"
__version__ = "1.0.1"
__maintainer__ = "TBD"
__email__ = "finkelelisabeth@gmail.com"
__status__ = "Prototype"
__date__ = "2019-07-03"
__updated__ = "2019-08-01"


def test_meal_time(meal=None):
    '''Print some relevant times for the dict `meal`'''
    if meal is None:
        meal = {
            'id': 4,
            'time': '17:00:00',
            'uid': 1,
            'primary_loc': 4,
            'weight': 0.016000,
            'stdev_time': '5:37:30'
        }
    loc_id = meal.get('primary_loc')
    loc = repo.get_loc(loc_id)
    tz = datetime.datetime.strptime(json.loads(loc['utc_offset']), '%z').tzinfo
    cur_loc_t = datetime.datetime.now(tz).time()
    print('Time at location '+str(loc_id)+': '+str(cur_loc_t))
    print('Meal time: '+meal['time'])
    print('Meal stdev: '+meal['stdev_time'])
    stdevs_off = repo.time_stdevs_off(meal, cur_loc_t)
    print('stdevs off: {0:.2f}'.format(stdevs_off))
    # return stdevs_off


# def print_similar_meals():
    


if __name__ == "__main__":
    test_meal_time()

